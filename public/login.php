<?php
session_start();
include_once '../conn/connect.php';
require_once '../app/functions.php';
include '../app/loginUser.php';

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial scale=1.0">
    <title>Login Page</title>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="../assets/login.css">
</head>

<body>
    <div class="container-fluid">
        <div class="login_container col-md-4 col-md-offset-4">
            <h3 class="text-center">Најави се</h3>
            <form method="POST" action="">
                <div class="form-group">
                    <label for="email">email</label>
                    <input type="email" class="form-control" id="email" placeholder="email" name="email">
                </div>
                <div class="form-group">
                    <label for="password">Лозинка</label>
                    <input type="password" class="form-control" id="password" placeholder="Лозинка" name="password">
                </div>
                <div>
                    <button type="submit" class="btn my_btn btn_yellow pull-right" name="submit">Најави се</button>
                    <a class="btn my_btn btn_yellow pull-right" href="index.php">Назад</a>
                </div>
            </form>
        </div>
    </div>
</body>

<html>