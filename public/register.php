<?php
session_start();

include_once '../app/functions.php';
require_once '../conn/connect.php';
include '../app/registerUser.php';




?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial scale=1.0">
    <title>Register Page</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="../assets/register.css">
</head>

<body>
    <div class="container-fluid">
        <div class="reg_container col-md-4 col-md-offset-4">
            <h3 class="text-center">Регистрирај се</h3>
            <form method="POST" action="">
                <div class="form-group">
                    <label for="name">Име *</label>
                    <input type="text" class="form-control" id="name" placeholder="Име" name="name">
                </div>
                <div class="form-group">
                    <label for="lastname">Презиме *</label>
                    <input type="text" class="form-control" id="lastname" placeholder="Презиме" name="lastname">
                </div>
                <div class="form-group">
                    <label for="company">Компанија *</label>
                    <input type="text" class="form-control" id="company" placeholder="Компанија" name="company">
                </div>
                <div class="form-group">
                    <label for="email">email *</label>
                    <input type="email" class="form-control" id="email" placeholder="email" name="email">
                </div>
                <div class="form-group">
                    <label for="password">Лозинка *</label>
                    <input type="password" class="form-control" id="password" placeholder="Лозинка" name="password">
                </div>
                <div class="form-group">
                    <label for="phone-number">Телефонски број *</label>
                    <input type="text" class="form-control" id="phone-number" placeholder="Телефонски број" name="phone-number">
                </div>
                <div class="form-group">
                    <label for="no-of-employees">Број на вработени *</label>
                    <select class="form-control" id="no-of-employees" name="no-of-employees">
                        <option value="1">1</option>
                        <option value="2-10">2-10</option>
                        <option value="11-50">11-50</option>
                        <option value="51-200">51-200</option>
                        <option value="200+">200+</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="sector">Сектор *</label>
                    <select class="form-control" id="sector" name="sector">
                        <option value="hr">Човечки ресурси</option>
                        <option value="marketing">Маркетинг</option>
                        <option value="product">Продукт</option>
                        <option value="sales">Продажба</option>
                        <option value="ceo">СЕО</option>
                        <option value="other">Друго</option>
                    </select>
                </div>
                <label for="text-area">Порака</label>
                <textarea class="form-control" rows="3" id="text-area" name="text-area"></textarea>
                <div>
                    <button type="submit" class="btn my_btn btn_yellow pull-right" name="submit">Регистрирај се</button>
                    <a class="btn my_btn btn_yellow pull-right" href="index.php">Назад</a>
                </div>
            </form>
        </div>
    </div>
</body>

<html>