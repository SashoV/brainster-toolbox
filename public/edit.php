<?php

session_start();

require_once '../conn/connect.php';
include '../app/editUser.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial scale=1.0">
    <title>Brainster ToolBox | Home</title>
    <script src="https://kit.fontawesome.com/e0b18861b4.js" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="../assets/edit.css" rel="stylesheet">
</head>

<body>
    <div class="container-fluid">
        <div class="col-md-4 col-md-offset-4">
            <h3 class="text-center">Промени ги податоците</h3>
            <form method="POST" action="">
                <div class="form-group">
                    <label for="name">Име</label>
                    <input type="text" class="form-control" id="name" value="<?= $row['name'] ?>" name="name">
                </div>
                <div class="form-group">
                    <label for="lastname">Презиме</label>
                    <input type="text" class="form-control" id="lastname" value="<?= $row['lastname'] ?>" name="lastname">
                </div>
                <div class="form-group">
                    <label for="company">Компанија</label>
                    <input type="text" class="form-control" id="company" value="<?= $row['company'] ?>" name="company">
                </div>
                <div class="form-group">
                    <label for="email">email</label>
                    <input type="email" class="form-control" id="email" value="<?= $row['email'] ?>" name="email">
                </div>
                <div class="form-group">
                    <label for="phone-number">Телефонски број</label>
                    <input type="text" class="form-control" id="phone-number" value="<?= $row['phone_number'] ?>" name="phone-number">
                </div>
                <div class="form-group">
                    <label for="no-of-employees">Број на вработени</label>
                    <select class="form-control" id="no-of-employees" name="no-of-employees">
                        <option value="1">1</option>
                        <option value="2-10">2-10</option>
                        <option value="11-50">11-50</option>
                        <option value="51-200">51-200</option>
                        <option value="200+">200+</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="sector">Сектор</label>
                    <select class="form-control" id="sector" name="sector">
                        <option value="hr">Човечки ресурси</option>
                        <option value="marketing">Маркетинг</option>
                        <option value="product">Продукт</option>
                        <option value="sales">Продажба</option>
                        <option value="ceo">СЕО</option>
                        <option value="other">Друго</option>
                    </select>
                </div>
                <label for="text-area">Порака</label>
                <textarea class="form-control" rows="3" id="text-area" name="text-area"><?= $row['message'] ?></textarea>
                <div>
                    <button type="submit" class="btn my_btn btn_yellow pull-right" name="submit">Зачувај ги промените</button>
                    <a class="btn my_btn btn_yellow pull-right" href="welcome.php">Назад</a>
                </div>
            </form>
        </div>
    </div>
</body>

</html>