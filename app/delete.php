<?php 
session_start();
require_once '../conn/connect.php';

if (!isset($_SESSION['id'])) { 
    header("Location: ../public/index.php");
    die();
}

$userId = $_SESSION['id'];

$sql = "DELETE FROM users WHERE id=:id LIMIT 1";
$stmt = $pdo->prepare($sql);


if($stmt->execute(['id' => $userId])) {
    session_destroy();
    header("Location: ../public/index.php?status=deleted-profile");
    die();
} 
