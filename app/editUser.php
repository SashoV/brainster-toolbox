<?php

require_once '../conn/connect.php';

if (!isset($_SESSION['id'])) {
    header("Location: ../public/index.php");
    die();
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if (isset($_POST['submit'])) {
        $userId = $_SESSION['id'];
        $newName = $_POST['name'];
        $newLastname = $_POST['lastname'];
        $newCompany = $_POST['company'];
        $newEmail = $_POST['email'];
        $newPhoneNumber = $_POST['phone-number'];
        $newNoOfEmployees = $_POST['no-of-employees'];
        $newSector = $_POST['sector'];
        $newMessage = $_POST['text-area'];


        $sql = "UPDATE users SET name=:name, lastname=:lastname, company=:company, email=:email, phone_number=:phone_number, no_of_employees=:no_of_employees, sector=:sector, message=:message WHERE id=:id";
        $stmt = $pdo->prepare($sql);

        $data = ['name' => $newName, 'lastname' => $newLastname, 'company' => $newCompany, 'email' => $newEmail, 'phone_number' => $newPhoneNumber, 'no_of_employees' => $newNoOfEmployees, 'sector' => $newSector, 'message' => $newMessage, 'id' => $userId];

        if ($stmt->execute($data)) {
            header("Location: ../public/welcome.php?status=success");
            die();
        }

        header("Location: ../public/edit.php?status=error");
        die();
    }
}

$userId = $_SESSION['id'];
$sql = "SELECT * FROM users WHERE id=:id";
$stmt = $pdo->prepare($sql);
$stmt->execute(['id' => $userId]);
$row = $stmt->fetch();