<?php

include_once '../app/functions.php';
require_once '../conn/connect.php';

checkIdSession();


if (isset($_POST['submit'])) {
    checkLoginFields();
    checkLoginEmail();
    $email = $_POST['email'];
    $password = md5($_POST['password']);

    $sql = "SELECT * FROM users WHERE email = :email AND password = :password";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['email' => $email, 'password' => $password]);

    $userCount = $stmt->rowCount();

    if ($userCount == 1) {
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        $_SESSION['id'] = $user['id'];
        $_SESSION['user_type'] = $user['user_type'];
        header("Location: ../public/welcome.php");
        die();
    }

    header("Location: ../public/login.php?error=wrongcredentials");
}