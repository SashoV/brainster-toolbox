<?php

include_once '../app/functions.php';
require_once '../conn/connect.php';

checkIfRegistered();


if (isset($_POST['submit'])) {
    checkRegisterFields();
    checkRegisterEmail();
    $name = $_POST['name'];
    $lastname = $_POST['lastname'];
    $company = $_POST['company'];
    $email = $_POST['email'];
    $phoneNumber = $_POST['phone-number'];
    $noOfEmployees = $_POST['no-of-employees'];
    $sector = $_POST['sector'];
    $message = $_POST['text-area'];
    $password = md5($_POST['password']);


    $sql = "SELECT * FROM users WHERE email = :email";
    $stmt = $pdo->prepare($sql);

    $stmt->bindParam('email', $email);
    $stmt->execute();

    if ($stmt->rowCount() == 0) {

        $sql = "INSERT INTO users(name, lastname, password, company, email, phone_number, no_of_employees, sector, message) 
                VALUES (:name, :lastname, :password, :company, :email, :phone_number, :no_of_employees, :sector, :message)";

        $stmt = $pdo->prepare($sql);

        $data = ['name' => $name, 'lastname' => $lastname, 'password' => $password, 'company' => $company, 'email' => $email, 'phone_number' => $phoneNumber, 'no_of_employees' => $noOfEmployees, 'sector' => $sector, 'message' => $message];
        $result = $stmt->execute($data);

        if ($result) {
            $sql = "SELECT * FROM users WHERE email = :email";
            $stmt = $pdo->prepare($sql);
            $stmt->execute(['email' => $email]);

            $userCount = $stmt->rowCount();

            if ($userCount == 1) {
                $user = $stmt->fetch(PDO::FETCH_ASSOC);
                $_SESSION['id'] = $user['id'];
                $_SESSION['user_type'] = $user['user_type'];
                header("Location: welcome.php");
                die();
            }
        }

        logMessage(json_encode($stmt->errorInfo()));
        header("Location: register.php?error=general");
        die();
    } else {
        header("Location: register.php?error=emailregistered");
        die();
    }
}