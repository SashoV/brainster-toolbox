<?php

function logMessage($msg)
{
    $logMessage = date("Y-m-d H:i:s", time()) . ": " . $msg . "\n";
    file_put_contents("../log.txt", $logMessage, FILE_APPEND);
}

function checkModalEmail()
{
    if (empty($_POST['email'])) {
        header("Location: ../public/index.php?danger=required");
        die();
    }
}

function isEmailValid()
{
    if (!preg_match("/^(?=[^@]{4,}@)([\w\.-]*[a-zA-Z0-9_]@(?=.{4,}\.[^.]*$)[\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z])$/", strtolower($_POST['email']))) {
        header("Location: ../public/index.php?danger=notvalidemail");
        die();
    }
}

function checkRegisterEmail()
{
    if (!preg_match("/^(?=[^@]{4,}@)([\w\.-]*[a-zA-Z0-9_]@(?=.{4,}\.[^.]*$)[\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z])$/", strtolower($_POST['email']))) {
        header("Location: ../public/register.php?danger=notvalidemail");
        die();
    }
}

function checkLoginEmail()
{
    if (!preg_match("/^(?=[^@]{4,}@)([\w\.-]*[a-zA-Z0-9_]@(?=.{4,}\.[^.]*$)[\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z])$/", strtolower($_POST['email']))) {
        header("Location: ../public/login.php?danger=notvalidemail");
        die();
    }
}

function checkLoginFields()
{
    if (empty(strtolower($_POST['email'])) || empty($_POST['password'])) {
        header("Location: ../public/login.php?danger=required");
        die();
    }
}

function checkRegisterFields()
{
    if (empty($_POST['name']) || empty($_POST['lastname']) || empty($_POST['company']) || empty($_POST['email']) || empty($_POST['password']) || empty($_POST['phone-number']) || empty($_POST['no-of-employees']) || empty($_POST['sector'])) {
        header("Location: ../public/register.php?danger=required");
        die();
    }
}

function checkIfRegistered()
{
    if (isset($_SESSION['id'])) {
        header("Location: welcome.php?error=already-registered");
        die();
    }
}

function checkIdSession()
{
    if (isset($_SESSION['id'])) {
        header("Location: ../public/welcome.php");
        die();
    }
}

function checkEmailSession()
{
    if (!isset($_SESSION['email']) || filter_var($_GET['game-id'], FILTER_VALIDATE_INT) === false || $_GET['game-id'] < 1 || $_GET['game-id'] > 18) {
        header("Location: ../public/index.php");
        die();
    }
}

function checkRole()
{
    if ($_SESSION['user_type'] !== "1") {
        header('Location: index.php');
    }
}

function createGameContent($pdo)
{
    $data = $pdo->query("SELECT * FROM games WHERE id = " . $_GET['game-id'] . "");
    foreach ($data as $row) {
        $headline = $row['headline'];
        $category = $row['category'];
        $timeFrame = $row['time_frame'];
        $groupSize = $row['group_size'];
        $facLevel = $row['fac_level'];
        $gameText = $row['game_text'];
        $materials = $row['materials'];
    }
}
